<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>web service</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Scada" rel="stylesheet"> 
    <!-- custom css -->
    <link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>

<!-- header -->
  <div class="wrapper-navbar-and-carousel">
  	<nav class="navbar navbar-default header-top">
  		<div class="container padding-header">
  			<div class="row">
  				<!-- logo brand -->
  				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
  					<div class="navbar-header">
  		              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
  		                <span class="sr-only">Toggle navigation</span>
  		                <span class="icon-bar"></span>
  		                <span class="icon-bar"></span>
  		                <span class="icon-bar"></span>
  		              </button>
  		              <a class="navbar-brand" href="">
  		                <img id="logo-brand" class="img-responsive" alt="Brand" src="image/brand.png">
  		              </a>
  		            </div>
  				</div>
  				<!-- navbar -->
  				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
  					<div class="collapse navbar-collapse" id="myNavbar">
  						<ul class="nav navbar-nav float"> 
  							<li><a href="">WORK</a></li>
  							<li><a href="">ABOUT</a></li>
  							<li><a href="">SERVICE</a></li>
  							<li><a href="">BLOG</a></li>
  							<li><a href="">CONTACT</a></li>
  						</ul>
  					</div>
  				</div>
  			</div>
  		</div>
  	</nav>

  <!-- carousel -->

  <div id="img-slide" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#img-slide" data-slide-to="0" class="active"></li>
      <li data-target="#img-slide" data-slide-to="1"></li>
      <li data-target="#img-slide" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="image/1.jpg" alt="...">
        <div class="carousel-caption">
          <div class="full-width text center desc-carousel">
          	<h1>We do wordpress development</h1>
          </div>
        </div>
      </div>
      <div class="item">
        <img src="image/2.jpg" alt="...">
        <div class="carousel-caption">
          <div class="full-width text center desc-carousel">
          	<h1>& Graphic designer</h1>
          </div>
        </div>
      </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  </div>

  <!-- service -->

  <div id="service-session">
    <section class="content-web bg-img">
      <div class="container service-section">
        <div class="wrapper">
          <div class="row"> 
            <div class="col-lg-12">
              <div class="title we-can-help-you">
                <div class="line"></div>
                <h1 style="margin-top: 10px;">We can help you</h1>
              </div>
            </div>
          </div>
          <div class="row row-table">
            <div class="content-wrap">
              <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 content-image-text">
                <a href="">
                  <div class="col-content">
                    <figure>
                      <img src="image/wp.png">
                    </figure>
                    <article>
                      <p>Wordpress website</p>
                    </article>
                  </div>
                </a>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 content-image-text">
                <a href="">
                  <div class="col-content">
                    <figure>
                      <img src="image/clog.png">
                    </figure>
                    <article>
                      <p>Creative logo</p>
                    </article>
                  </div>
                </a>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 content-image-text">
                <a href="">
                  <div class="col-content">
                    <figure>
                      <img src="image/poster.png">
                    </figure>
                    <article>
                      <p>Poster design</p>
                    </article>
                  </div>
                </a>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 content-image-text">
                <a href="">
                  <div class="col-content">
                    <figure>
                      <img src="image/card.png">
                    </figure>
                    <article>
                      <p>Bussiness card & etc</p>
                    </article>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <!-- project -->


  <div class="container">
    <div class="wrapper-all-content-project">
      <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <div class="title projects-desc">
              <div class="line-project"></div>
              <h1 style="margin-top: 10px; color: #989898;">Projects</h1>
            </div> 
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="view-all-work">
              <a class="btn btn-primary" href="">View All Projects</a>
            </div>
          </div>
      </div>
        <div class="wrapper-content-project"> 
          <div class="row">
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="image/mac.jpg" alt="..." >
                <div class="caption">
                  <h3 style="margin-top: 15px;">Apple</h3>
                  <p>Komputer mac</p>
                  <p><a href="#" class="btn btn-default btn-xs" role="button">Read More</a></p>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="image/mac2.jpg" alt="...">
                <div class="caption">
                  <h3 style="margin-top: 15px;">iMac</h3>
                  <p>Komputer mac</p>
                  <p><a href="#" class="btn btn-default btn-xs" role="button">Read More</a></p>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="image/mac3.jpg" alt="..." >
                <div class="caption">
                  <h3 style="margin-top: 15px;">Technology</h3>
                  <p>iMac komputer</p>
                  <p><a href="#" class="btn btn-default btn-xs" role="button">Read More</a></p>
                </div>
              </div>
            </div>
             <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="image/mac4.jpg" alt="..." >
                <div class="caption">
                  <h3 style="margin-top: 15px;">Apple</h3>
                  <p>Komputer mac</p>
                  <p><a href="#" class="btn btn-default btn-xs" role="button">Read More</a></p>
                </div>
              </div>
            </div>
             <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="image/mac5.jpg" alt="..." >
                <div class="caption">
                  <h3 style="margin-top: 15px;">Apple</h3>
                  <p>Komputer mac</p>
                  <p><a href="#" class="btn btn-default btn-xs" role="button">Read More</a></p>
                </div>
              </div>
            </div>
             <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="image/mac6.jpg" alt="..." >
                <div class="caption">
                  <h3 style="margin-top: 15px;">Apple</h3>
                  <p>Komputer mac</p>
                  <p><a href="#" class="btn btn-default btn-xs" role="button">Read More</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
          <div class="wrapper-button-hidden">
            <div class="row">
              <div class="col-xs-12">
                <div class="view-all-work-hidden">
                  <a class="btn btn-info" href="">View All Projects</a>
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>

<!-- blog -->

  <div class="wrapper-blog-home">
    <div class="container">
      <div class="content-blog-home">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <div class="title from-our-blog">
              <div class="line-blog"></div>
              <h1 style="margin-top: 10px; color: #989898;">From our blogs</h1>
            </div> 
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="view-all-work">
              <a class="btn btn-primary" href="">View All Blog</a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="thumbnail blog-home">
              <a href="">
                <img src="image/mac5.jpg" alt="..." >
                <div class="caption">
                  <h3 style="margin-top: 15px;">Technology</h3>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="thumbnail blog-home">
              <a href="">
                <img src="image/mac5.jpg" alt="..." >
                <div class="caption">
                  <h3 style="margin-top: 15px;">Technology</h3>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="thumbnail blog-home">
              <a href="">
                <img src="image/mac5.jpg" alt="..." >
                <div class="caption">
                  <h3 style="margin-top: 15px;">Technology</h3>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="thumbnail blog-home">
              <a href="">
                <img src="image/mac5.jpg" alt="..." >
                <div class="caption">
                  <h3 style="margin-top: 15px;">Technology</h3>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="wrapper-button-hidden">
            <div class="row">
              <div class="col-xs-12">
                <div class="view-all-work-hidden">
                  <a class="btn btn-info" href="">View All Blog</a>
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>


<!-- footer -->

<div id="wrapper-footer">
  <div class="bg-img-footer">
    <div class="container">
      <div class="wrapper-content-footer">
        <div class="wrapper-title">
          <div class="row">
            <div class="col-lg-12">
              <div class="title-footer">
                <h1>OUR BRAND</h1>
              </div>
            </div>
          </div>
        </div>
        <div class="wrapper-nav-footer">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div class="wrapper-nav">
                <div class="title-nav">
                  <div class="line-nav"></div>
                  <h3>FOLLOW</h3>
                </div>
                <div class="nav-content">
                  <a href="">NAV</a>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div class="wrapper-nav">
                <div class="title-nav">
                  <div class="line-nav"></div>
                  <h3>OFFICE</h3>
                </div>
                <div class="nav-content">
                  <a href="">Yogyakarta</a>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div class="wrapper-nav">
                <div class="title-nav">
                  <div class="line-nav"></div>
                  <h3>USEFUL</h3>
                </div>
                <div class="nav-content">
                  <a href="">Privacy</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="wrapper-copyright text-center">
            <h5>Copyright by deni</h5>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>